﻿using UnityEngine;
using System.Collections;

public class PaddleControl : MonoBehaviour {

	public KeyCode moveUp;
	public KeyCode moveDown;

	public bool canMoveUp = true;
	public bool canMoveDown = true;

	public Camera mainCamera;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Input.GetKey(moveUp))
		{
			if (canMoveUp)
				rigidbody2D.velocity = new Vector2(0, 10f);
			//gameObject.transform.position = new Vector2(gameObject.transform.position.x, gameObject.transform.position.y + .1f);

		}
		else if (Input.GetKey(moveDown))
		{
			if (canMoveDown)
				gameObject.transform.position = new Vector2(gameObject.transform.position.x, gameObject.transform.position.y + -.1f);
		}
		else
		{
			rigidbody2D.velocity = new Vector2(0,0);
		}

	}

	void OnCollisionEnter2D(Collision2D info)
	{
		Debug.Log(info.collider.name);
		if (info.collider.name == "topWall")
			canMoveUp = false;
		if (info.collider.name == "bottomWall")
			canMoveDown = false;
	}

	void OnCollisionExit2D(Collision2D info)
	{
		canMoveUp = true;
		canMoveDown = true;
	}

}
