﻿using UnityEngine;
using System.Collections;
using System;

public class rightWallControl : MonoBehaviour
{

	public GUIText scoreBoard;

	// Use this for initialization
	void Start ()
	{
		var score = Convert.ToInt32(scoreBoard.text);

		scoreBoard.text = score.ToString();
	}
	
	// Update is called once per frame
	void Update ()
	{
		var score = Convert.ToInt32(scoreBoard.text);

		score++;

		scoreBoard.text = score.ToString();

		var x = UnityEngine.Random.Range(10f, 15f);

		var y = UnityEngine.Random.Range(5f, 15f);

		var xFlip = UnityEngine.Random.Range(-2f, 3f);

		var yFlip = UnityEngine.Random.Range(-2f, 3f);


		x = x * (xFlip > 0 ? 1f : -1f);

		y = y * (yFlip > 0 ? 1f : -1f);

		rigidbody2D.velocity = new Vector2(x, y);
	}
}
