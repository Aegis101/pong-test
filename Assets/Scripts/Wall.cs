﻿using UnityEngine;
using System.Collections;
using System;

public class Wall : MonoBehaviour
{

	public GUIText scoreBoard;

	public GameObject ball;

	public GameObject rightPaddle;

	public GameObject leftPaddle;

	// Use this for initialization
	void Start () {
		var score = Convert.ToInt32(scoreBoard.text);

		scoreBoard.text = score.ToString();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter2D(Collision2D info)
	{
		Debug.Log(info.gameObject.name);
		if (scoreBoard != null)
		{
			var score = Convert.ToInt32(scoreBoard.text);

			score++;

			scoreBoard.text = score.ToString();

			ball.transform.position = new Vector3(0, 0, 0);

			var x = UnityEngine.Random.Range(13f, 20f);

			var y = UnityEngine.Random.Range(5f, 15f);

			var xFlip = UnityEngine.Random.Range(-2f, 3f);

			var yFlip = UnityEngine.Random.Range(-2f, 3f);


			x = x*(xFlip > 0 ? 1f : -1f);

			y = y*(yFlip > 0 ? 1f : -1f);

			ball.rigidbody2D.velocity = new Vector2(0, 0);

			ball.rigidbody2D.AddForce(new Vector2(x, y));
		}
	}
}
