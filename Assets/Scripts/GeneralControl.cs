﻿using UnityEngine;
using System.Collections;

public class GeneralControl : MonoBehaviour
{
	public Camera mainCamera;

	public BoxCollider2D topWall;
	public BoxCollider2D bottomWall;
	public BoxCollider2D rightWall;
	public BoxCollider2D leftWall;

	public GameObject playone;
	public GameObject playtwo;

	// Use this for initialization
	void Start () {

		topWall.center = new Vector2(0, mainCamera.ScreenToWorldPoint(new Vector3(0,Screen.height,0)).y + .5f);
		topWall.size = new Vector2(mainCamera.ScreenToWorldPoint(new Vector3(Screen.width * 2f,0,0)).x, 1f);

		bottomWall.center = new Vector2(0, mainCamera.ScreenToWorldPoint(new Vector3(0,0, 0)).y - .5f);
		bottomWall.size = new Vector2(mainCamera.ScreenToWorldPoint(new Vector3(Screen.width * 2f, 0, 0)).x, 1f);

		rightWall.center = new Vector2(mainCamera.ScreenToWorldPoint(new Vector3(Screen.width, 0, 0)).x + .5f, 0);
		rightWall.size = new Vector2(1f, mainCamera.ScreenToWorldPoint(new Vector3(0, Screen.height * 2f, 0)).y);

		leftWall.center = new Vector2(mainCamera.ScreenToWorldPoint(new Vector3(0, 0, 0)).x - .5f, 0);
		leftWall.size = new Vector2(1f, mainCamera.ScreenToWorldPoint(new Vector3(0, Screen.height * 2f, 0)).y);

		playone.transform.position = new Vector3(mainCamera.ScreenToWorldPoint(new Vector3(75f, 0, 0)).x,0,0);

		playtwo.transform.position = new Vector3(mainCamera.ScreenToWorldPoint(new Vector3(Screen.width - 75f, 0, 0)).x, 0, 0);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
