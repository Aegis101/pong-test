﻿using UnityEngine;
using System.Collections;

public class BallController : MonoBehaviour {

	// Use this for initialization
	void Start ()
	{
		var x = Random.Range(13f, 20f);
		var y = Random.Range(5f, 15f);
		
		var xFlip = Random.Range(-2f, 3f);
		var yFlip = Random.Range(-2f, 3f);
		
		x = x * (xFlip > 0 ? 1f : -1f);
		y = y * (yFlip > 0 ? 1f : -1f);
		
		rigidbody2D.AddForce(new Vector2(x, y));
	}
	
	// Update is called once per frame
	void Update () 
	{

	}
}
